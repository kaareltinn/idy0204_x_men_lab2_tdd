import React from 'react'
import { shallow } from 'enzyme'
import TravelList from './TravelList'
import TravelItem from './TravelItem'

const travels = [
  { id: 1, departure: 'Tartu', destination: 'Tallinn' },
  { id: 2, departure: 'Tallinn', destination: 'Tartu' },
  { id: 3, departure: 'Tartu', destination: 'Elva' },
  { id: 4, departure: 'Pärnu', destination: 'Põlva' },
  { id: 5, departure: 'Viljandi', destination: 'Narva' }
]

it('renders travels', () => {
  const list = shallow(<TravelList travels={travels}/>)
  expect(list.find(TravelItem)).toHaveLength(5)
})

it('filters travels on departure input', () => {
  const list = shallow(<TravelList travels={travels}/>)

  list.find('.departure-search').simulate('change', { target: { value: 'Ta' }})

  expect(list.find(TravelItem)).toHaveLength(3)
})

it('filters travels on destination input', () => {
  const list = shallow(<TravelList travels={travels}/>)

  list.find('.destination-search').simulate('change', { target: { value: 'Ta' }})

  expect(list.find(TravelItem)).toHaveLength(2)
})

it('filters travels on departure AND destination input', () => {
  const list = shallow(<TravelList travels={travels}/>)

  list.find('.departure-search').simulate('change', { target: { value: 'Vi' }})
  list.find('.destination-search').simulate('change', { target: { value: 'Põ' }})

  expect(list.find(TravelItem)).toHaveLength(0)
})
