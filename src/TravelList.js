import React, { Component } from 'react'
import TravelItem from './TravelItem'
import './TravelList.css'

class TravelList extends Component {
  constructor(props) {
    super(props)
    this.state = {
      departure: '',
      destination: '',
      travels: props.travels
    }
  }

  onDepartureChange(props) {
    return ((event) => {
      this.setState(
        { departure: event.target.value },
        () => this.filterTravels(props.travels)
      )
    })
  }

  onDestinationChange(props) {
    return ((event) => {
      this.setState(
        { destination: event.target.value },
        () => this.filterTravels(props.travels)
      )
    })
  }

  filterTravels(travels) {
    this.setState({
      travels: travels.filter((travel) => {
        return travel.departure.includes(this.state.departure) && travel.destination.includes(this.state.destination)
      })
    })
  }

  render() {
    return (
      <div className="travels-list">
        Väljumine: <input className="departure-search search" onChange={this.onDepartureChange(this.props)} />
        Sihtkoht: <input className="destination-search search" onChange={this.onDestinationChange(this.props)} />
        {
          this.state.travels.map((travel) => {
            return (
              <TravelItem key={travel.id} travel={travel} />
            )
          })
        }
      </div>
    )
  }
}

export default TravelList
