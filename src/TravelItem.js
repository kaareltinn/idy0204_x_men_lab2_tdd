import React, { Component } from 'react'
import './TravelItem.css'

class TravelItem extends Component {
  render() {
    return (
      <div className="travel-item">
        <span className="departure">Väljumine: {this.props.travel.departure}</span>
        <span className="destination">Sihtkoht: {this.props.travel.destination}</span>
      </div>
    )
  }
}

export default TravelItem
