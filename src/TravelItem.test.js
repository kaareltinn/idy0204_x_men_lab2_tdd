import React from 'react'
import { shallow } from 'enzyme'
import toJson from 'enzyme-to-json'
import TravelItem from './TravelItem'

it('renders withour crashing', () => {
  const travel = {
    departure: 'Foo',
    destination: 'Bar'
  }
  const wrapper = shallow(<TravelItem travel={travel} />)

  expect(toJson(wrapper)).toMatchSnapshot()
})
