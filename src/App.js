import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';
import TravelList from './TravelList'

const travels = [
  { id: 1, departure: 'Tartu', destination: 'Tallinn' },
  { id: 2, departure: 'Tallinn', destination: 'Tartu' },
  { id: 3, departure: 'Tartu', destination: 'Elva' },
  { id: 4, departure: 'Pärnu', destination: 'Põlva' },
  { id: 5, departure: 'Viljandi', destination: 'Narva' }
]

class App extends Component {
  render() {
    return (
      <div className="App">
        <TravelList travels={travels}/>
      </div>
    );
  }
}

export default App;
